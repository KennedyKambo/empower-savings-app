package org.busaracenter.empowersavings.widgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import org.busaracenter.empowersavings.R;

public class BusaraButton extends AppCompatButton {
    public BusaraButton(Context context) {
        super(context);
    }

    public BusaraButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BusaraButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setBackground(Drawable background) {
        super.setBackground(getResources().getDrawable(R.drawable.background_buton));
    }

    @Override
    public void setTextColor(int color) {
        super.setTextColor(getResources().getColor(R.color.textDark));
    }
}

package org.busaracenter.empowersavings.adapters;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.busaracenter.empowersavings.R;
import org.busaracenter.empowersavings.activities.Helper;
import org.busaracenter.empowersavings.activities.Summary;
import org.busaracenter.empowersavings.pojos.Queries;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static org.busaracenter.empowersavings.utilities.UtilsManager.getTimeStamp;
import static org.busaracenter.empowersavings.utilities.UtilsManager.myToast;
import static org.busaracenter.empowersavings.utilities.UtilsManager.show;

public class QueryAdapter extends RecyclerView.Adapter<QueryAdapter.viewHolder> {

    private ArrayList<Queries> items;
    private JSONObject jsonObject;

    //adapter to populate recyclerView widget in Amounts class
    public QueryAdapter(ArrayList<Queries> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_child, viewGroup, false);

        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {
        viewHolder.imageView1.setImageResource(items.get(i).image1);
        viewHolder.imageView2.setImageResource(items.get(i).nextIcon);

        viewHolder.textView1.setText(items.get(i).title);
        viewHolder.textView2.setText(items.get(i).description);
    }

    @Override
    public int getItemCount() {
        return (null != items ? items.size() : 0);
    }

    class viewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ConstraintLayout constraintLayout;
        ImageView imageView1;
        ImageView imageView2;
        TextView textView1;
        TextView textView2;
        MaterialDialog dialog;
        SharedPreferences sharedPreferences;
        int position;
        String saving, phone, goal, type;


        viewHolder(@NonNull View itemView) {
            super(itemView);
            constraintLayout = itemView.findViewById(R.id.layout);
            imageView1 = itemView.findViewById(R.id.first_image);
            imageView2 = itemView.findViewById(R.id.second_image);
            textView1 = itemView.findViewById(R.id.first_text);
            textView2 = itemView.findViewById(R.id.second_text);
            position = getAdapterPosition() + 1;
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(itemView.getContext());
            phone = sharedPreferences.getString("Phone", "");
            goal = sharedPreferences.getString("Goal", "");
            constraintLayout.setOnClickListener(this);
        }

        //setting values
        @Override
        public void onClick(View view) {
            position = getAdapterPosition();
            if (position == 0) {
                saving = "500";
                type = "SAVVY SAVER";
            } else if (position == 1) {
                saving = "1000";
                type = "POWER SAVER";
            } else if (position == 2) {
                saving = "2000";
                type = "SUPER SAVER";
            } else if (position == 3) {
                saving = "5000";
                type = "CHAMPION SAVER";
            }
            createGoal();
        }

        //saves values to sharedPreferences
        void savePrefs() {
            final SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
            Intent intent = new Intent(itemView.getContext(), Summary.class);
            position = getAdapterPosition();
            if (position == 0) {
                saving = "500";
                type = "SAVVY SAVER";
                prefsEditor.putString("Saving", "500");
                prefsEditor.putString("Type", type);
                intent.putExtra("Drawable", "image_savvy");
            } else if (position == 1) {
                saving = "1000";
                type = "POWER SAVER";
                prefsEditor.putString("Saving", "1000");
                prefsEditor.putString("Type", type);
                intent.putExtra("Drawable", "image_power");
            } else if (position == 2) {
                saving = "2000";
                type = "SUPER SAVER";
                prefsEditor.putString("Saving", "2000");
                prefsEditor.putString("Type", type);
                intent.putExtra("Drawable", "image_super");
            } else if (position == 3) {
                saving = "5000";
                type = "CHAMPION SAVER";
                prefsEditor.putString("Saving", "5000");
                prefsEditor.putString("Type", type);
                intent.putExtra("Drawable", "image_champ");
            }
            itemView.getContext().startActivity(intent);
            prefsEditor.apply();
        }

        //creates new goals
        void createGoal() {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(itemView.getContext())
                    .progress(true, 100)
                    .content("Loading...")
                    .cancelable(false);
            dialog = builder.build();
            dialog.show();

            AndroidNetworking.post(Helper.Header + "apicall=createsavings")
                    .addBodyParameter("phone", phone)
                    .addBodyParameter("goal", goal)
                    .addBodyParameter("type", type)
                    .addBodyParameter("saving", saving)
                    .addBodyParameter("saved", "0")
                    .addBodyParameter("tymstamp", getTimeStamp())
                    .addBodyParameter("purpose", Helper.Purpose)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONArray jsonArray;
                            try {
                                jsonArray = response.getJSONArray("savings");
                                jsonObject = jsonArray.getJSONObject(jsonArray.length() - 1);
                                Helper.Id = String.valueOf(jsonObject.getInt("id"));
                                Helper.Phone = jsonObject.getString("phone");
                                Helper.Goal = jsonObject.getString("goal");
                                Helper.Type = jsonObject.getString("type");
                                Helper.MySaving = jsonObject.getInt("saving");
                                Helper.MySaved = jsonObject.getInt("saved");
                                Helper.Balance = Helper.MySaving - Helper.MySaved;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            show(myToast("Success..." + ("\ud83d\ude03"), Gravity.CENTER, itemView.getContext()).getView(), itemView.getContext());
                            savePrefs();
                            dialog.cancel();
                        }

                        @Override
                        public void onError(ANError error) {
                            Toast.makeText(itemView.getContext(), R.string.check_connection, Toast.LENGTH_LONG).show();
                            dialog.cancel();
                        }
                    });
        }
    }
}

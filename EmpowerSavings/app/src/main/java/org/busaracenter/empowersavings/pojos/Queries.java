package org.busaracenter.empowersavings.pojos;

public class Queries {

    public String title, description;
    public int image1, nextIcon, block;

    public Queries(String title, String description, int image1, int nextIcon, int block) {
        this.title = title;
        this.description = description;
        this.image1 = image1;
        this.nextIcon = nextIcon;
        this.block = block;
    }

    public int getImage1() {
        return image1;
    }

    public void setImage1(int image1) {
        this.image1 = image1;
    }

    public int getNextIcon() {
        return nextIcon;
    }

    public void setNextIcon(int nextIcon) {
        this.nextIcon = nextIcon;
    }

    public int getBlock() {
        return block;
    }

    public void setBlock(int block) {
        this.block = block;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

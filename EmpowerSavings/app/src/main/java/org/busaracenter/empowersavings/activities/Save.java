package org.busaracenter.empowersavings.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ScrollView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.busaracenter.empowersavings.R;
import org.busaracenter.empowersavings.widgets.BusaraButton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static org.busaracenter.empowersavings.utilities.UtilsManager.coloredSlash;
import static org.busaracenter.empowersavings.utilities.UtilsManager.getTimeStamp;
import static org.busaracenter.empowersavings.utilities.UtilsManager.myToast;
import static org.busaracenter.empowersavings.utilities.UtilsManager.shakeError;
import static org.busaracenter.empowersavings.utilities.UtilsManager.show;

public class Save extends AppCompatActivity {
    private static String logs;
    public BusaraButton but_save, but_logs, but_elog, but_exit;
    private AppCompatEditText specific, amount;
    private ConstraintLayout constraintLayout;
    private ScrollView scrollView;
    private AppCompatTextView textView;
    private ArrayList<String> stringArrayList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //layout and widget initialisation
        setContentView(R.layout.activity_save);
        specific = findViewById(R.id.specific);
        amount = findViewById(R.id.phone);
        but_save = findViewById(R.id.but_save);
        but_logs = findViewById(R.id.view_logs);
        but_elog = findViewById(R.id.exit_logs);
        but_exit = findViewById(R.id.but_exit);
        constraintLayout = findViewById(R.id.save_layout);
        scrollView = findViewById(R.id.logs_layout);
        textView = findViewById(R.id.logs_view);
        CheckBox checkBox1 = findViewById(R.id.savings);
        CheckBox checkBox2 = findViewById(R.id.fixed_deposit);
        CheckBox checkBox3 = findViewById(R.id.digital_savings);
        CheckBox checkBox4 = findViewById(R.id.kolo_box);
        CheckBox checkBox5 = findViewById(R.id.bond);
        CheckBox checkBox6 = findViewById(R.id.money_mutual);


        but_save.setEnabled(true);

        //finishes the current activity
        but_exit.setOnClickListener(view -> finish());

        //makes logs view visible and hides savings view
        but_logs.setOnClickListener(view -> {
            constraintLayout.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
            getLogs(Integer.parseInt(Helper.Id));
        });

        but_elog.setOnClickListener(view -> {
            constraintLayout.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
        });

        //creates an arrayList of checkBox widgets so as to use for each loop to loop through the List and check for clicked widgets
        ArrayList<CheckBox> checkBoxes = new ArrayList<>();
        stringArrayList = new ArrayList<>();
        checkBoxes.add(checkBox1);
        checkBoxes.add(checkBox2);
        checkBoxes.add(checkBox3);
        checkBoxes.add(checkBox4);
        checkBoxes.add(checkBox5);
        checkBoxes.add(checkBox6);

        //checks for all checkBox widgets that have been clicked and adds string value attached to each
        //into the arrayList to pass as parameter into the updateGoals method
        for (CheckBox checkBox : checkBoxes) {
            checkBox.setOnClickListener(view -> {
                if (checkBox.isChecked()) {
                    stringArrayList.add(checkBox.getText().toString());
                } else {
                    stringArrayList.remove(checkBox.getText().toString());
                }
            });
        }

        AppCompatImageView pencil = findViewById(R.id.pencil);
        AppCompatTextView others = findViewById(R.id.others_text);
        others.setText(R.string.others_please_specify);
        pencil.setBackgroundResource(R.drawable.vector_edit_vh);

        //sets custom vehicle editText widget to visible
        pencil.setOnClickListener(view -> {
            //radioGroup.clearCheck();
            if (specific.getVisibility() == View.GONE) {
                specific.setVisibility(View.VISIBLE);
                specific.requestFocus();
            } else {
                specific.setVisibility(View.GONE);
            }

        });
        //sets custom vehicle editText widget to visible
        others.setOnClickListener(view -> {
            //radioGroup.clearCheck();
            if (specific.getVisibility() == View.GONE) {
                specific.setVisibility(View.VISIBLE);
                specific.requestFocus();
            } else {
                specific.setVisibility(View.GONE);
            }
        });

        but_save.setOnClickListener(view -> {

            //conditionals that check if conditions such as amount entered to save  is greater than balance,
            //equal to zero, or vehicle is not present and displays appropriate messages.The methods to update goals
            //and savings are called if the conditions set are met
            if (Objects.requireNonNull(specific.getText()).length() > 1 && !specific.getText().toString().equals("")) {
                stringArrayList.add(specific.getText().toString());
            }

            if (Objects.requireNonNull(amount.getText()).toString().equals("") || amount.getText().length() == 0 || Integer.parseInt(String.valueOf(amount.getText())) < 1) {
                show(myToast(getResources().getString(R.string.lesser_text), Gravity.BOTTOM, getApplicationContext()).getView(), getApplicationContext());
                amount.setAnimation(shakeError());
            } else if (Integer.parseInt(String.valueOf(amount.getText())) > Helper.Balance) {
                show(myToast(getResources().getString(R.string.larger_amount), Gravity.BOTTOM, getApplicationContext()).getView(), getApplicationContext());
                amount.setAnimation(shakeError());
            } else if (stringArrayList.size() < 1 && Objects.requireNonNull(specific.getText()).toString().equals("") || Objects.requireNonNull(specific.getText()).toString().equals(" ")) {
                show(myToast(getResources().getString(R.string.select_vehicle), Gravity.BOTTOM, getApplicationContext()).getView(), getApplicationContext());
            } else {
                but_save.setEnabled(false);
                updateData(Integer.parseInt(String.valueOf(amount.getText())));
            }
        });
        super.onCreate(savedInstanceState);
    }

    //method to update user savings in the savings table.Takes amount saved as parameter.
    //Other values passed in are obtained from the static class Helper
    void updateData(int saved) {
        AndroidNetworking.post(Helper.Header + "apicall=updatesavings")
                .addBodyParameter("id", Helper.Id)
                .addBodyParameter("phone", Helper.Phone)
                .addBodyParameter("goal", Helper.Goal)
                .addBodyParameter("type", Helper.Type)
                .addBodyParameter("saving", String.valueOf(Helper.MySaving))
                .addBodyParameter("saved", String.valueOf(saved + Helper.MySaved))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("savings");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Helper.MySaved = jsonObject.getInt("saved");
                                Helper.Balance = jsonObject.getInt("saving") - jsonObject.getInt("saved");
                            }
                            //converts all vehicle entries into a string to pass into the method updateGoals as vehicle parameter
                            String vehicles = stringArrayList.toString().replace("[", "").replace("]", "");
                            //clears list after update to prevent sending to lists into one entry
                            stringArrayList.clear();
                            updateGoals(vehicles, Integer.parseInt(Objects.requireNonNull(amount.getText()).toString()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(getApplicationContext(), R.string.check_connection, Toast.LENGTH_LONG).show();
                    }
                });
    }

    //method to update selected individual goals in the goals table,takes the vehicle and amount saved as parameters
    //other values passed into actual apiCall are obtained from static UtilsManager class methods  and Helper class
    void updateGoals(String vehicle, int saved) {
        AndroidNetworking.post(Helper.Header + "apicall=updategoals")
                .addBodyParameter("goal_id", Helper.Id)
                .addBodyParameter("saved", String.valueOf(saved))
                .addBodyParameter("vehicle", vehicle)
                .addBodyParameter("tymstamp", getTimeStamp())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //if else loop that determines from where Save.Class has been launched so as to either finish it if not intent
                        //i.e if class has been called from the status bar notification, or to refresh the class if intent message is from Charts class
                        //final portion of loop calls Home class,finishes the current Save class and passes congratulatory message as toast
                        if (getIntent().getExtras() == null) {
                            finish();
                        } else if (getIntent().getStringExtra("ClassName").equals("Charts")) {
                            refresh(saved);
                        } else {
                            String goal = Helper.Goal;
                            Toast.makeText(getApplicationContext(), "Congratulations, you have saved Naira " + saved + " towards your " + goal + " goal.", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(getApplicationContext(), R.string.check_connection, Toast.LENGTH_LONG).show();
                    }
                });
    }

    //method to refresh the activity so current savings reflect and replace the previous savings
    //uses alarmManager class to schedule the class Home's relaunch after 100 milliseconds
    //also sends the amount saved and the goal concerned to Home class for congratulatory message
    void refresh(int saved) {
        Intent intent = new Intent(getApplicationContext(), Home.class);
        intent.putExtra("Saved", saved);
        intent.putExtra("Goal", Helper.Goal);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        System.exit(0);
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        assert alarmManager != null;
        alarmManager.set(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis() + 100, PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_CANCEL_CURRENT));
    }

    //method to get user logs over time i.e amount saved,time saved,vehicle used e.t.c
    void getLogs(int id) {
        AndroidNetworking.post(Helper.Header + "apicall=getlogs")
                .addBodyParameter("goal_id", String.valueOf(id))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String refined;
                        try {
                            logs = response.toString(2);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String s = logs.replace("{", "").replace("tymstamp", "Time");
                        String r = s.replace("},", "").replace("\"", "");
                        String t = r.replace(", ", coloredSlash()).replace("goal_id", "User Id");
                        String u = t.replace(":[", "").replace("Logs", "").replace(",", "");
                        String v = u.replace("]", "").replace("vehicle", "Vehicle");
                        String w = v.replace("}", "").replace("saved", "Saved");
                        refined = w.replace("id", "Goal Id").replace(":", " - ");
                        textView.setText(refined);
                        textView.setTextColor(getResources().getColor(R.color.green));
                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(getApplicationContext(), R.string.check_connection, Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
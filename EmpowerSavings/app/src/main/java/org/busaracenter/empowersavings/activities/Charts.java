package org.busaracenter.empowersavings.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import org.busaracenter.empowersavings.R;
import org.busaracenter.empowersavings.adapters.FragsAdapter;
import org.busaracenter.empowersavings.fragments.GroupFrag;
import org.busaracenter.empowersavings.fragments.OwnerFrag;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Charts extends AppCompatActivity {
    ViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //layout and widget initialisation
        setContentView(R.layout.activity_charts);
        viewPager = findViewById(R.id.view_pager);

        //setting up the viewPager widget
        setupViewPager(viewPager);

        //attribute set and parameter set to one to prevent both fragments from loading chart data at the same time
        viewPager.setOffscreenPageLimit(1);
        super.onCreate(savedInstanceState);
    }

    private void setupViewPager(ViewPager viewPager) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragsAdapter adapter = new FragsAdapter(fragmentManager);
        adapter.addFragment(new OwnerFrag(), "Self");
        adapter.addFragment(new GroupFrag(), "Group");

        viewPager.setAdapter(adapter);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

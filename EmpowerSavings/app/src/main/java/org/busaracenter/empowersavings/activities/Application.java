package org.busaracenter.empowersavings.activities;

import com.androidnetworking.AndroidNetworking;

import org.busaracenter.empowersavings.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        //initialising networking library
        AndroidNetworking.initialize(getApplicationContext());

        //Creating and initialising app font work
        //Done at application level to apply to all activities in same application
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/candy1.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        super.onCreate();
    }
}

package org.busaracenter.empowersavings.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;

import org.busaracenter.empowersavings.R;

import java.util.Objects;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static org.busaracenter.empowersavings.utilities.UtilsManager.shakeError;

public class Login extends AppCompatActivity {

    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private ScrollView scrollView;
    private AppCompatEditText editText;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //layout and widget initialisation
        setContentView(R.layout.activity_login);
        editText = findViewById(R.id.phone);
        scrollView = findViewById(R.id.login_form);
        button = findViewById(R.id.email_sign_in_button);

        editText.setOnFocusChangeListener((view, hasFocus) -> {
            if (hasFocus)
                scrollToView(scrollView, button);
        });

        //find reference to imageView widget in loginActivity and make it round
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.image_logo);
        RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getResources(), bm);
        roundDrawable.setCircular(true);
        imageView.setImageDrawable(roundDrawable);

        //request for app permissions
        if (Build.VERSION.SDK_INT >= 28) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.WAKE_LOCK, Manifest.permission.FOREGROUND_SERVICE}, 100);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.WAKE_LOCK}, 100);
        }

        button.setOnClickListener(view -> attemptLogin());
    }

    //checks if conditions for logIn are met and proceeds to logIn if they are
    private void attemptLogin() {
        if (Objects.requireNonNull(editText.getText()).length() < 9) {
            editText.setError("Must be at least 9 characters long");
            editText.setAnimation(shakeError());
        } else if (Objects.requireNonNull(editText.getText()).length() > 12) {
            editText.setError("Can't be longer than 12 characters");
            editText.setAnimation(shakeError());
        } else {
            Intent intent = new Intent(this, Home.class);
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            editor = sharedPreferences.edit();
            editor.putString("Phone", editText.getText().toString());
            editor.apply();
            startActivity(intent);
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    //scrolls editText widget in its parent to allow full visibility of the same
    private void scrollToView(final ScrollView scrollViewParent, final View view) {
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        scrollViewParent.smoothScrollTo(0, childOffset.y);
    }

    //scrolls editText widget in its parent to allow full visibility of the same
    private void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}


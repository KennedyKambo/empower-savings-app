package org.busaracenter.empowersavings.utilities;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;
import android.widget.Toast;

import org.busaracenter.empowersavings.R;
import org.busaracenter.empowersavings.activities.Helper;
import org.busaracenter.empowersavings.activities.Home;
import org.busaracenter.empowersavings.activities.Save;
import org.busaracenter.empowersavings.pojos.Objects;
import org.busaracenter.empowersavings.receivers.NotificationReceiver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static android.content.Context.ALARM_SERVICE;

public class UtilsManager {

    private static NotificationChannel notificationChannel;
    private static NotificationManager notificationManager;

    public UtilsManager() {

    }

    //timestamp to include in goal creation for logging purposes
    public static String getTimeStamp() {
        Date presentTime_Date = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormat.format(presentTime_Date);
    }

    //weekly notifications
    public static void showNotification(Objects objects, Context context, String message, int Id) {
        String title = context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
        if (notificationManager == null) {
            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationCompat.Builder builder;
            if (message.equalsIgnoreCase("Congratulations!\nYou have successfully completed your mission.Lets go pick another one.")) {
                intent = new Intent(context, Home.class);
            } else {
                intent = new Intent(context, Save.class);
            }
            Helper.Id = String.valueOf(objects.getId());
            Helper.Phone = objects.getNumber();
            Helper.Goal = objects.getGoal();
            Helper.Type = objects.getType();
            Helper.MySaving = objects.getSaving();
            Helper.MySaved = objects.getSaved();
            Helper.Balance = objects.getSaving() - objects.getSaved();

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent;
            int importance = NotificationManager.IMPORTANCE_HIGH;
            if (notificationChannel == null) {
                notificationChannel = new NotificationChannel("0", title, importance);
                notificationChannel.setDescription(message);
                notificationChannel.enableVibration(true);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            builder = new NotificationCompat.Builder(context, "0");

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(context, Id, intent, PendingIntent.FLAG_ONE_SHOT);
            builder.setContentTitle(title)
                    .setSmallIcon(getNotificationIcon())
                    .setContentText(message)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(false)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setBadgeIconType(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            Notification notification = builder.build();
            notificationManager.notify(Id, notification);
        } else {
            if (message.equalsIgnoreCase("Congratulations!\nYou have successfully completed your mission.Lets go pick another one.")) {
                intent = new Intent(context, Home.class);
            } else {
                intent = new Intent(context, Save.class);
            }
            Helper.Id = String.valueOf(objects.getId());
            Helper.Phone = objects.getNumber();
            Helper.Goal = objects.getGoal();
            Helper.Type = objects.getType();
            Helper.MySaving = objects.getSaving();
            Helper.MySaved = objects.getSaved();
            Helper.Balance = objects.getSaving() - objects.getSaved();

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent;

            pendingIntent = PendingIntent.getActivity(context, Id, intent, PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(false)
                    .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    .setSound(defaultSoundUri)
                    .setSmallIcon(getNotificationIcon())
                    .setContentIntent(pendingIntent)
                    .setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(title).bigText(message));

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.notify(Id, notificationBuilder.build());
        }
    }

    //white badge icon for android LOLLIPOP and above
    private static int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }

    //different notification messages for different cases of goal completion
    public static String notificationMessage(Objects objects) {
        int remainingWeeks = Integer.parseInt(calculateDays()) / 7;
        int balancePerWeek;
        if (remainingWeeks > 1) {
            balancePerWeek = (objects.getSaving() - objects.getSaved()) / remainingWeeks;
        } else {
            balancePerWeek = (objects.getSaving() - objects.getSaved());
        }
        if (balancePerWeek == 0) {
            return "Congratulations!\nYou have successfully completed your mission.Lets go pick another one.";
        } else if (remainingWeeks > 1) {
            return "You have a goal of N " + objects.getSaving() + " .You have saved N " + objects.getSaved() + " if you save N " + balancePerWeek + " per week you'll be able to achieve your goal.";
        } else {
            return "You have a goal of N " + objects.getSaving() + " .You have saved N " + objects.getSaved() + " if you save N " + balancePerWeek + " this week you'll be able to achieve your goal.";
        }
    }

    //method to calculate days remaining for goal setting month to end
    public static String calculateDays() {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int date = calendar.get(Calendar.DAY_OF_MONTH);
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        return String.valueOf(maxDay - date);
    }

    //method to extract the toast's viewGroup to apply alignment and typeface
    public static void show(View view, Context context) {
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        for (int j = 0; j < childCount; j++) {
            int count = viewGroup.getChildCount();
            for (int i = 0; i < count; i++) {
                View child = viewGroup.getChildAt(i);
                if (child instanceof TextView) {
                    ((TextView) child).setTypeface(UtilsManager.candy1(context));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        child.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    }
                }
            }
        }
    }

    //custom toast with varied gravity for better presentation
    public static Toast myToast(String message, int gravity, Context context) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        if (gravity == Gravity.BOTTOM) {
            toast.setGravity(gravity, 0, 20);
        } else {
            toast.setGravity(gravity, 0, 0);
        }
        toast.show();
        return toast;
    }

    //custom typeface for better ui
    private static Typeface candy1(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/candy1.otf");
    }

    //spanned slash for better ui.Used in congratulatory message
    public static Spanned coloredSlash() {
        return Html.fromHtml("<html><body><span style=color:b> / <lue/span></body></html>");
    }

    //shake error used in editTexts when criteria is not met
    public static TranslateAnimation shakeError() {
        TranslateAnimation translateAnimation = new TranslateAnimation(10, 0, 0, 0);
        translateAnimation.setInterpolator(new CycleInterpolator(7));
        translateAnimation.setDuration(500);
        return translateAnimation;
    }

    //alarm settings
    //set to repeat on a weekly basis
    //has NotificationReceiver as pendingIntent
    public static long setRepeatingAlarm(Context context) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
        calendar.set(Calendar.HOUR_OF_DAY, 9);
        calendar.set(Calendar.MINUTE, 30);
        calendar.set(Calendar.SECOND, 0);

        Intent intent = new Intent(context, NotificationReceiver.class);
        intent.setAction("ac.in.ActivityRecognition.RestartSensor");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        java.util.Objects.requireNonNull(am);
        am.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, pendingIntent);

        return calendar.getTimeInMillis();
    }
}
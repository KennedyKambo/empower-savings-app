package org.busaracenter.empowersavings.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.widget.ImageView;

import org.busaracenter.empowersavings.R;
import org.busaracenter.empowersavings.widgets.BusaraButton;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Objects;
import java.util.TimeZone;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Description extends AppCompatActivity {
    ImageView imageView, imageView2, imageView1;
    AppCompatTextView textView1, textView2;
    AppCompatEditText editText;
    BusaraButton button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //layout and widget initialisation
        setContentView(R.layout.activity_desc);
        imageView = findViewById(R.id.imageView3);
        imageView2 = findViewById(R.id.imageView2);
        imageView1 = findViewById(R.id.home);
        button = findViewById(R.id.but_set);
        textView1 = findViewById(R.id.appCompatTextView6);
        textView2 = findViewById(R.id.appCompatTextView4);
        editText = findViewById(R.id.for_what);

        //getting user goal from sharedPreferences.Saved earlier in
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String title = sharedPreferences.getString("Goal", "");

        //getting drawable to use in imageView from intent
        String name = getIntent().getStringExtra("Drawable");
        imageView.setImageResource(getResources().getIdentifier(name, "drawable", getPackageName()));

        //setting goal description texts to textView widgets
        textView2.setText(MessageFormat.format(getString(R.string.text_chosen) + " " + currentMonth() + " " + getString(R.string.text_goal_in_mind), title));
        textView1.setText(title);

        //adding home and close methods to home and close imageView widgets
        imageView2.setOnClickListener(view -> finish());
        imageView1.setOnClickListener(view -> {
            Intent intent = new Intent(this, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });

        button.setOnClickListener(view -> openAmountsClass());
        super.onCreate(savedInstanceState);
    }

    //opens amounts class and passes few messages via intent to use as Header and message
    public void openAmountsClass() {
        Intent intent = new Intent(this, Amounts.class);
        intent.putExtra("Header", "SAVING TOWARDS\n" + textView1.getText().toString());
        intent.putExtra("Message", textView2.getText().toString());
        if (Objects.requireNonNull(editText.getText()).length() > 0) {
            Helper.Purpose = Objects.requireNonNull(editText.getText()).toString();
        } else {
            Helper.Purpose = " ";
        }
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    //method to find current gal setting month based on local calendar
    public String currentMonth() {
        String month = "";
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        switch (calendar.get(Calendar.MONTH)) {
            case 0:
                month = "January";
                break;
            case 1:
                month = "February";
                break;
            case 2:
                month = "March";
                break;
            case 3:
                month = "April";
                break;
            case 4:
                month = "May";
                break;
            case 5:
                month = "June";
                break;
            case 6:
                month = "July";
                break;
            case 7:
                month = "August";
                break;
            case 8:
                month = "September";
                break;
            case 9:
                month = "October";
                break;
            case 10:
                month = "November";
                break;
            case 11:
                month = "December";
                break;
        }
        return month;
    }
}

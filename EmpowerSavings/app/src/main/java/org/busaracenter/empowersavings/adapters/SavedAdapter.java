package org.busaracenter.empowersavings.adapters;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.busaracenter.empowersavings.R;
import org.busaracenter.empowersavings.activities.Charts;
import org.busaracenter.empowersavings.activities.Helper;
import org.busaracenter.empowersavings.pojos.Objects;

import java.util.ArrayList;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class SavedAdapter extends RecyclerView.Adapter<SavedAdapter.viewHolder> {
    public static ArrayList<Objects> objects;

    public SavedAdapter(ArrayList<Objects> objects) {
        SavedAdapter.objects = objects;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_mine, viewGroup, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {
        viewHolder.textView1.setText(String.valueOf(objects.get(i).getNumber()));
        viewHolder.textView2.setText(objects.get(i).getGoal());
        viewHolder.textView3.setText(objects.get(i).getType());
        viewHolder.textView4.setText(String.valueOf(objects.get(i).getSaving()));
        viewHolder.textView5.setText(String.valueOf(objects.get(i).getSaved()));
        viewHolder.textView6.setText(String.valueOf(objects.get(i).getId()));
        viewHolder.textView7.setText(String.valueOf(objects.get(i).getPurpose()));

        if (viewHolder.constraintLayout.getVisibility() == View.VISIBLE && viewHolder.getAdapterPosition() == 0) {
            new MaterialShowcaseView.Builder((Activity) viewHolder.itemView.getContext())
                    .setTarget(viewHolder.constraintLayout)
                    .setDismissText("GOT IT")
                    .setContentText("You can click on such cards for more options")
                    .setDelay(50)
                    .singleUse("saved_cards")
                    .show();

        }
        switch (viewHolder.textView3.getText().toString()) {
            case "SAVVY SAVER":
                viewHolder.imageView1.setImageResource(R.drawable.image_savvy);
                break;
            case "POWER SAVER":
                viewHolder.imageView1.setImageResource(R.drawable.image_power);
                break;
            case "SUPER SAVER":
                viewHolder.imageView1.setImageResource(R.drawable.image_super);
                break;
            case "CHAMPION SAVER":
                viewHolder.imageView1.setImageResource(R.drawable.image_champ);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return (null != objects ? objects.size() : 0);
    }

    public class viewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        CardView constraintLayout;
        ImageView imageView1;
        AppCompatTextView textView1, textView2, textView3, textView4, textView5, textView6, textView7;
        viewHolder(@NonNull View itemView) {
            super(itemView);
            constraintLayout = itemView.findViewById(R.id.layout);
            imageView1 = itemView.findViewById(R.id.type_image);
            textView1 = itemView.findViewById(R.id.number_value);
            textView2 = itemView.findViewById(R.id.goal_value);
            textView3 = itemView.findViewById(R.id.type_value);
            textView4 = itemView.findViewById(R.id.saving_value);
            textView5 = itemView.findViewById(R.id.saved_value);
            textView6 = itemView.findViewById(R.id.id_value);
            textView7 = itemView.findViewById(R.id.purpose_value);
            constraintLayout.setOnClickListener(this);
            constraintLayout.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(itemView.getContext(), Charts.class);
            Helper.Position = getAdapterPosition();
            Helper.Id = String.valueOf(objects.get(getAdapterPosition()).getId());
            Helper.Phone = objects.get(getAdapterPosition()).getNumber();
            Helper.Goal = objects.get(getAdapterPosition()).getGoal();
            Helper.Type = objects.get(getAdapterPosition()).getType();
            Helper.MySaving = objects.get(getAdapterPosition()).getSaving();
            Helper.MySaved = objects.get(getAdapterPosition()).getSaved();
            Helper.Balance = objects.get(getAdapterPosition()).getSaving() - objects.get(getAdapterPosition()).getSaved();
            itemView.getContext().startActivity(intent);
        }

        @Override
        public boolean onLongClick(View view) {
            return true;
        }
    }
}

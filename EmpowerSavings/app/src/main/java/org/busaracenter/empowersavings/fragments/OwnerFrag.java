package org.busaracenter.empowersavings.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;

import org.busaracenter.empowersavings.R;
import org.busaracenter.empowersavings.activities.Helper;
import org.busaracenter.empowersavings.activities.Home;
import org.busaracenter.empowersavings.activities.Save;
import org.busaracenter.empowersavings.adapters.SavedAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import uk.co.deanwild.materialshowcaseview.IShowcaseListener;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

import static org.busaracenter.empowersavings.utilities.UtilsManager.calculateDays;

public class OwnerFrag extends Fragment {
    private static int mysaving, mysaved;
    MaterialDialog dialog;
    TextView saved, remaining, days;
    AppCompatButton but_save, but_exit;
    ImageView chevron;
    SharedPreferences sharedPreferences;
    int shown;

    public OwnerFrag() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view1 = inflater.inflate(R.layout.activity_self, container, false);
        saved = view1.findViewById(R.id.saved_value);
        remaining = view1.findViewById(R.id.remainder_value);
        days = view1.findViewById(R.id.days_value);
        but_save = view1.findViewById(R.id.but_save);
        but_exit = view1.findViewById(R.id.but_exit);
        chevron = view1.findViewById(R.id.chevron);

        saved.setText(String.valueOf(SavedAdapter.objects.get(Helper.Position).getSaved()));
        remaining.setText(String.valueOf(SavedAdapter.objects.get(Helper.Position).getSaving() - SavedAdapter.objects.get(Helper.Position).getSaved()));
        days.setText(calculateDays());

        sharedPreferences = Objects.requireNonNull(getContext()).getSharedPreferences("material_showcaseview_prefs", Context.MODE_PRIVATE);
        shown = sharedPreferences.getInt("status_swipe_for_groups", 0);

        if (shown == -1) {
            chevron.setVisibility(View.GONE);
        } else {
            new MaterialShowcaseView.Builder((Activity) getContext())
                    .setTarget(chevron)
                    .setDismissText("GOT IT")
                    .setListener(new IShowcaseListener() {
                        @Override
                        public void onShowcaseDisplayed(MaterialShowcaseView materialShowcaseView) {

                        }

                        @Override
                        public void onShowcaseDismissed(MaterialShowcaseView materialShowcaseView) {
                            chevron.setVisibility(View.GONE);
                        }
                    })
                    .setContentText("Swipe left to see your groups progress.")
                    .setDelay(50)
                    .singleUse("swipe_for_groups")
                    .show();
        }

        but_exit.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });
        but_save.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), Save.class);
            intent.putExtra("ClassName", "Charts");
            startActivity(intent);
        });

        getMyStats(view1);
        return view1;
    }

    void getMyStats(final View view) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(Objects.requireNonNull(getContext()))
                .progress(true, 100)
                .content("Loading...")
                .cancelable(false);
        dialog = builder.build();
        dialog.show();
        AndroidNetworking.post(Helper.Header + "apicall=getmystats")
                .addBodyParameter("id", String.valueOf(SavedAdapter.objects.get(Helper.Position).getId()))
                .addBodyParameter("goal", SavedAdapter.objects.get(Helper.Position).getGoal())
                .addBodyParameter("type", SavedAdapter.objects.get(Helper.Position).getType())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray;
                        dialog.cancel();
                        mysaving = 0;
                        mysaved = 0;
                        try {
                            jsonArray = response.getJSONArray("savings");
                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject obj = (JSONObject) jsonArray.get(j);
                                mysaving += Integer.parseInt(obj.getString("saving"));
                                mysaved += Integer.parseInt(obj.getString("saved"));
                            }
                            PieChart pieChart = view.findViewById(R.id.graph);
                            pieChart.setUsePercentValues(true);
                            pieChart.setTransparentCircleRadius(30f);
                            pieChart.setDrawSlicesUnderHole(true);
                            pieChart.setHoleRadius(80f);
                            ArrayList<Entry> yvalues = new ArrayList<>();
                            float percentage3 = (float) mysaved / mysaving * 100.0f;
                            float percentage1 = 100.0f - percentage3;
                            yvalues.add(new Entry(percentage1, 0));
                            yvalues.add(new Entry(percentage3, 1));
                            PieDataSet dataSet = new PieDataSet(yvalues, "");
                            dataSet.setColors(new int[]{R.color.grey, R.color.green}, getContext());
                            ArrayList<String> xVals = new ArrayList<>();
                            xVals.add("Remaining");
                            xVals.add("Saved");
                            PieData data = new PieData(xVals, dataSet);
                            data.setValueFormatter(new PercentFormatter());
                            pieChart.setData(data);
                            pieChart.setCenterText("" + percentage3 + "%\n" + "Saved");
                            pieChart.animateXY(1400, 1400);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        dialog.cancel();
                        Toast.makeText(getContext(), R.string.check_connection, Toast.LENGTH_LONG).show();
                    }
                });
    }
}

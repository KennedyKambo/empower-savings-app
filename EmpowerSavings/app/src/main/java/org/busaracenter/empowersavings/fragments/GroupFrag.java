package org.busaracenter.empowersavings.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;

import org.busaracenter.empowersavings.R;
import org.busaracenter.empowersavings.activities.Helper;
import org.busaracenter.empowersavings.adapters.SavedAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static org.busaracenter.empowersavings.utilities.UtilsManager.calculateDays;

public class GroupFrag extends Fragment {
    private static int total, total1;
    private TextView saved, remaining, days, members;

    public GroupFrag() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view1 = inflater.inflate(R.layout.activity_group, container, false);
        saved = view1.findViewById(R.id.saved_value);
        remaining = view1.findViewById(R.id.remainder_value);
        days = view1.findViewById(R.id.days_value);
        members = view1.findViewById(R.id.members_value);

        getGroupStats(view1);
        return view1;
    }

    void getGroupStats(final View view) {
        AndroidNetworking.post(Helper.Header + "apicall=getgroups")
                .addBodyParameter("goal", SavedAdapter.objects.get(Helper.Position).getGoal())
                .addBodyParameter("type", SavedAdapter.objects.get(Helper.Position).getType())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray = null;
                        total1 = 0;
                        total = 0;
                        try {
                            jsonArray = response.getJSONArray("savings");
                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject obj = (JSONObject) jsonArray.get(j);
                                total += Integer.parseInt(obj.getString("saving"));
                                total1 += Integer.parseInt(obj.getString("saved"));
                            }
                            Helper.GroupSaved = total1;
                            Helper.GroupSaving = total;

                            saved.setText(String.valueOf(total1));
                            remaining.setText(String.valueOf(total - total1));
                            days.setText(calculateDays());
                            members.setText(String.valueOf(jsonArray.length()));

                            PieChart pieChart = view.findViewById(R.id.graph);

                            pieChart.setUsePercentValues(true);
                            pieChart.setTransparentCircleRadius(30f);
                            pieChart.setHoleRadius(80f);

                            ArrayList<Entry> yvalues = new ArrayList<>();
                            float percentage = (float) Helper.GroupSaved / Helper.GroupSaving * 100.0f;
                            float percentage2 = 100.0f - percentage;
                            yvalues.add(new Entry(percentage, 0));
                            yvalues.add(new Entry(percentage2, 1));

                            PieDataSet dataSet = new PieDataSet(yvalues, "");
                            dataSet.setColors(new int[]{R.color.orange, R.color.grey}, getContext());

                            ArrayList<String> xVals = new ArrayList<>();

                            xVals.add("Saved");
                            xVals.add("Remaining");
                            PieData data = new PieData(xVals, dataSet);

                            data.setValueFormatter(new PercentFormatter());

                            pieChart.setData(data);
                            pieChart.setCenterText("" + percentage + "%\n" + "Saved");

                            pieChart.animateXY(1400, 1400);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(getContext(), R.string.check_connection, Toast.LENGTH_LONG).show();
                    }
                });
    }
}

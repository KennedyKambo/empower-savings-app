package org.busaracenter.empowersavings.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.busaracenter.empowersavings.R;
import org.busaracenter.empowersavings.adapters.QueryAdapter;
import org.busaracenter.empowersavings.pojos.Queries;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Amounts extends AppCompatActivity {
    AppCompatTextView set_goal, motto;
    RecyclerView rvGoals;
    ArrayList<Queries> items;
    QueryAdapter mainAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //layout and widget initialisation
        setContentView(R.layout.activity_goals);
        set_goal = findViewById(R.id.text_set_goal);
        motto = findViewById(R.id.text_motto);
        rvGoals = findViewById(R.id.rvNewgoals);

        //arrayList,recyclerView and Adapter initialisation,
        items = new ArrayList<>();
        items.add(new Queries(getString(R.string.savvy_saver), getString(R.string.savvy_value), R.drawable.image_savvy, R.drawable.image_next, 0));
        items.add(new Queries(getString(R.string.power_saver), getString(R.string.power_value), R.drawable.image_power, R.drawable.image_next, 0));
        items.add(new Queries(getString(R.string.super_saver), getString(R.string.super_value), R.drawable.image_super, R.drawable.image_next, 0));
        items.add(new Queries(getString(R.string.champion_saver), getString(R.string.champion_value), R.drawable.image_champ, R.drawable.image_next, 0));
        mainAdapter = new QueryAdapter(items);
        rvGoals.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        rvGoals.setAdapter(mainAdapter);
        rvGoals.setNestedScrollingEnabled(false);
        rvGoals.addItemDecoration(new Divider(getResources().getDrawable(R.drawable.vector_recy_dv)));

        //setting values to textView widgets
        set_goal.setText(getIntent().getStringExtra("Header"));
        motto.setText(getIntent().getStringExtra("Message"));

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

package org.busaracenter.empowersavings.pojos;

public class Objects {

    private int saving, saved, id;
    private String number, goal, type, purpose;

    public Objects(int id, String number, String goal, String type, String purpose, int saving, int saved) {
        this.number = number;
        this.saving = saving;
        this.saved = saved;
        this.goal = goal;
        this.type = type;
        this.id = id;
        this.purpose = purpose;
    }

    public Objects() {

    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getSaved() {
        return saved;
    }

    public void setSaved(int saved) {
        this.saved = saved;
    }

    public int getSaving() {
        return saving;
    }

    public void setSaving(int saving) {
        this.saving = saving;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }
}

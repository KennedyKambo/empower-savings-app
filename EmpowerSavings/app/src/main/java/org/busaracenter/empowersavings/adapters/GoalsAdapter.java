package org.busaracenter.empowersavings.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.busaracenter.empowersavings.R;
import org.busaracenter.empowersavings.activities.Description;
import org.busaracenter.empowersavings.pojos.Queries;

import java.util.ArrayList;

public class GoalsAdapter extends RecyclerView.Adapter<GoalsAdapter.viewHolder> {
    //adapter to populate recyclerView widget in Home class
    private ArrayList<Queries> items;
    private Context context;

    public GoalsAdapter(Context context, ArrayList<Queries> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_child, viewGroup, false);

        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder viewHolder, int i) {
        viewHolder.imageView1.setImageResource(items.get(i).image1);
        viewHolder.imageView2.setImageResource(items.get(i).nextIcon);
        viewHolder.textView1.setText(items.get(i).title);
        viewHolder.textView2.setText(items.get(i).description);
    }

    @Override
    public int getItemCount() {
        return (null != items ? items.size() : 0);
    }

    class viewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ConstraintLayout constraintLayout;
        ImageView imageView1;
        ImageView imageView2;
        TextView textView1;
        TextView textView2;

        viewHolder(@NonNull View itemView) {
            super(itemView);
            constraintLayout = itemView.findViewById(R.id.layout);
            imageView1 = itemView.findViewById(R.id.first_image);
            imageView2 = itemView.findViewById(R.id.second_image);
            textView1 = itemView.findViewById(R.id.first_text);
            textView2 = itemView.findViewById(R.id.second_text);
            constraintLayout.setOnClickListener(this);
        }

        //saves goal to sharedPreferences and opens a new intent with extras to Description class
        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            Intent intent = new Intent(itemView.getContext(), Description.class);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(itemView.getContext());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("Goal", items.get(pos).title);
            editor.apply();
            intent.putExtra("Description", items.get(pos).description);
            if (pos == 0) {
                intent.putExtra("Drawable", "image_bulb");
            } else if (pos == 1) {
                intent.putExtra("Drawable", "image_brief");
            } else {
                intent.putExtra("Drawable", "image_heart");
            }
            context.startActivity(intent);
        }
    }
}

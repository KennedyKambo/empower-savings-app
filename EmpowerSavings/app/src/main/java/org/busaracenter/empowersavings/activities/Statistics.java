package org.busaracenter.empowersavings.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;

import org.busaracenter.empowersavings.R;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static org.busaracenter.empowersavings.utilities.UtilsManager.calculateDays;

public class Statistics extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    AppCompatImageView imageView;
    AppCompatTextView textView, textView1, textView3, textView4, textView5, textView6, textView7, textView8, textView9;
    PieChart pieChart1, pieChart;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_stats);
        //layout,widget and variable initialisation
        imageView = findViewById(R.id.image_head);
        textView = findViewById(R.id.text_set_goal);
        textView1 = findViewById(R.id.text_motto);

        textView3 = findViewById(R.id.saved_value);
        textView3.setText(String.valueOf(Helper.MySaved));
        textView4 = findViewById(R.id.remainder_value);
        textView4.setText(String.valueOf(Helper.MySaving - Helper.MySaved));
        textView5 = findViewById(R.id.days_value);
        textView5.setText(calculateDays());

        textView6 = findViewById(R.id.group_saved_value);
        textView6.setText(String.valueOf(Helper.GroupSaved));
        textView7 = findViewById(R.id.group_remainder_value);
        textView7.setText(String.valueOf(Helper.GroupSaving - Helper.GroupSaved));
        textView8 = findViewById(R.id.group_days_value);
        textView8.setText(calculateDays());
        textView9 = findViewById(R.id.members_value);
        textView9.setText(String.valueOf(Helper.Members));

        pieChart = findViewById(R.id.piechart);
        pieChart1 = findViewById(R.id.piechart1);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        //Personal chart data settings
        ArrayList<Entry> yvalues1 = new ArrayList<>();
        float percentage3 = (float) Summary.mysaved / Summary.mysaving * 100.0f;
        float percentage1 = 100.0f - percentage3;
        yvalues1.add(new Entry(percentage1, 0));
        yvalues1.add(new Entry(percentage3, 1));
        PieDataSet dataSet1 = new PieDataSet(yvalues1, "");
        dataSet1.setColors(new int[]{R.color.grey, R.color.green}, this);
        ArrayList<String> xVals1 = new ArrayList<>();
        xVals1.add("Remaining");
        xVals1.add("Saved");
        PieData data1 = new PieData(xVals1, dataSet1);
        data1.setValueFormatter(new PercentFormatter());
        pieChart1.setData(data1);
        pieChart1.setTransparentCircleRadius(30f);
        pieChart1.setHoleRadius(80f);
        pieChart1.setUsePercentValues(true);
        pieChart1.setCenterText("" + percentage3 + "%\n" + "Saved");
        pieChart1.animateXY(1400, 1400);

        //Groups chart data settings
        ArrayList<Entry> yvalues = new ArrayList<>();
        float percentage = (float) Summary.groupsaved / Summary.groupsaving * 100.0f;
        float percentage2 = 100.0f - percentage;
        yvalues.add(new Entry(percentage, 0));
        yvalues.add(new Entry(percentage2, 1));
        PieDataSet dataSet = new PieDataSet(yvalues, "");
        dataSet.setColors(new int[]{R.color.orange, R.color.grey}, this);
        ArrayList<String> xVals = new ArrayList<>();
        xVals.add("Saved");
        xVals.add("Remaining");
        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        pieChart.setData(data);
        pieChart.setUsePercentValues(true);
        pieChart.setTransparentCircleRadius(30f);
        pieChart.setHoleRadius(80f);
        pieChart.setCenterText("" + percentage + "%\n" + "Saved");
        pieChart.animateXY(1400, 1400);

        //setting data to appropriate textView widgets
        String name = Helper.Image;
        imageView.setImageResource(getResources().getIdentifier(name, "drawable", getPackageName()));
        textView.setText(getIntent().getStringExtra("Title"));
        textView1.setText(String.format("You can now keep track of your individual progress on %s as well as your groups savings progress.Your group has " + String.valueOf(Helper.Members) + " members so far and you have " + calculateDays() + " days remaining to reach your savings goal.", getIntent().getStringExtra("Title").replace("\n", " ")));
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

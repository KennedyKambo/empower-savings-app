package org.busaracenter.empowersavings.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.busaracenter.empowersavings.activities.Home;
import org.busaracenter.empowersavings.pojos.Objects;

import java.util.ArrayList;

import static org.busaracenter.empowersavings.utilities.UtilsManager.notificationMessage;
import static org.busaracenter.empowersavings.utilities.UtilsManager.setRepeatingAlarm;
import static org.busaracenter.empowersavings.utilities.UtilsManager.showNotification;

public class NotificationReceiver extends BroadcastReceiver {
    ArrayList<Objects> objectsArrayList;

    @Override
    public void onReceive(Context context, Intent intent) {
        objectsArrayList = Home.getItems(context);
        /*Nested if else ensures if the intent action is from the system i.e boot up,and the
         time for notifications is not yet reached,the receiver does nothing but log itself;
        Otherwise it fires the notifications.
        The else ensures that if the action received points to our explicit action,
        then the notification fires immediately since it is triggered on purpose.
         */
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction()) /*|| "android.intent.action.LOCKED_BOOT_COMPLETED".equals(intent.getAction()) || "android.intent.action.QUICKBOOT_POWERON".equals(intent.getAction())*/) {
            if (System.currentTimeMillis() != setRepeatingAlarm(context)) {
                setRepeatingAlarm(context);
            } else {
                createNotifications(objectsArrayList, context);
            }
        } else if ("ACTION_DEVICE_IDLE_MODE_CHANGED".equals(intent.getAction())) {

        } else {
            createNotifications(objectsArrayList, context);
        }
    }

    //method to create locations based on number of items in the list passed in as a parameter
    public void createNotifications(ArrayList<Objects> objectsArrayList, Context context) {
        for (Objects objects : objectsArrayList) {
            showNotification(objects, context, notificationMessage(objects), objects.getId());
        }
    }
}

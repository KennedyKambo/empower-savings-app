package org.busaracenter.empowersavings.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.busaracenter.empowersavings.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Summary extends AppCompatActivity {
    static int groupsaving = 0, groupsaved = 0, mysaving = 0, mysaved = 0;
    AppCompatTextView textView, textView1;
    AppCompatImageView imageView, imageView1;
    AppCompatButton button, button1;
    MaterialDialog dialog;
    SharedPreferences sharedPreferences;
    JSONObject obj;
    JSONObject jsonObject;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //layout,widget and variable initialisation
        setContentView(R.layout.activity_mgoal);
        textView = findViewById(R.id.text_group_type);
        textView1 = findViewById(R.id.all_set);
        imageView = findViewById(R.id.image_goal);
        button = findViewById(R.id.appCompatButton);
        button1 = findViewById(R.id.appCompatButton2);
        imageView1 = findViewById(R.id.home);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        //setting data to appropriate textView and imageView widgets
        textView.setText(String.format("%s %s GROUP", sharedPreferences.getString("Type", " "), sharedPreferences.getString("Goal", " ")));
        textView1.setText(String.format("You're all set to start saving Naira %s", sharedPreferences.getString("Saving", " ")));
        getMyStats();
        imageView.setImageResource(getResources().getIdentifier(getIntent().getStringExtra("Drawable"), "drawable", getPackageName()));

        imageView1.setOnClickListener(view -> {
            Intent intent = new Intent(this, Home.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });

        //gets statistics
        button.setOnClickListener(view -> {
            getMyStats();
            getGroupStats();
        });

        //starts Save clas with intent message to serve as checkPoint in Save class
        button1.setOnClickListener(view -> {
            Intent intent = new Intent(Summary.this, Save.class);
            intent.putExtra("ClassName", "Summary");
            startActivity(intent);
        });

        super.onCreate(savedInstanceState);
    }

    //method to get group statistics from api endpoint
    public void getGroupStats() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(Summary.this)
                .progress(true, 100)
                .content("Loading...")
                .cancelable(false);
        dialog = builder.build();
        dialog.show();

        AndroidNetworking.post(Helper.Header + "apicall=getgroups")
                .addBodyParameter("goal", sharedPreferences.getString("Goal", ""))
                .addBodyParameter("type", sharedPreferences.getString("Type", ""))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray;
                        groupsaving = 0;
                        groupsaved = 0;
                        try {
                            jsonArray = response.getJSONArray("savings");
                            for (int j = 0; j < jsonArray.length(); j++) {
                                obj = (JSONObject) jsonArray.get(j);
                                groupsaving += Integer.parseInt(obj.getString("saving"));
                                groupsaved += Integer.parseInt(obj.getString("saved"));
                                Helper.Members = jsonArray.length();
                            }

                            Intent intent = new Intent(Summary.this, Statistics.class);
                            intent.putExtra("Title", textView.getText().toString());
                            intent.putExtra("ClassName", "Summary");
                            if (textView.getText().toString().contains("SAVVY SAVER")) {
                                Helper.Image = "image_savvy";
                            } else if (textView.getText().toString().contains("POWER SAVER")) {
                                Helper.Image = "image_power";
                            } else if (textView.getText().toString().contains("SUPER SAVER")) {
                                Helper.Image = "image_super";
                            } else {
                                Helper.Image = "image_champ";
                            }
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Helper.GroupSaving = groupsaving;
                        Helper.GroupSaved = groupsaved;
                        dialog.cancel();
                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(getApplicationContext(), R.string.check_connection, Toast.LENGTH_LONG).show();
                        dialog.cancel();
                    }
                });
    }

    //method to get user statistics fom api endpoint
    public void getMyStats() {
        AndroidNetworking.post(Helper.Header + "apicall=getmystats")
                .addBodyParameter("id", Helper.Id)
                .addBodyParameter("goal", sharedPreferences.getString("Goal", ""))
                .addBodyParameter("type", sharedPreferences.getString("Type", ""))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray;
                        mysaved = 0;
                        mysaving = 0;
                        try {
                            jsonArray = response.getJSONArray("savings");
                            obj = (JSONObject) jsonArray.get(0);
                            jsonObject = obj;
                            mysaving += Integer.parseInt(obj.getString("saving"));
                            mysaved += Integer.parseInt(obj.getString("saved"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Helper.MySaving = mysaving;
                        Helper.MySaved = mysaved;
                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(getApplicationContext(), R.string.check_connection, Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}

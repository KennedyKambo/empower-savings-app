package org.busaracenter.empowersavings.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.busaracenter.empowersavings.R;
import org.busaracenter.empowersavings.adapters.GoalsAdapter;
import org.busaracenter.empowersavings.adapters.MonthAdapter;
import org.busaracenter.empowersavings.adapters.SavedAdapter;
import org.busaracenter.empowersavings.pojos.Objects;
import org.busaracenter.empowersavings.pojos.Queries;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static org.busaracenter.empowersavings.utilities.UtilsManager.myToast;
import static org.busaracenter.empowersavings.utilities.UtilsManager.setRepeatingAlarm;
import static org.busaracenter.empowersavings.utilities.UtilsManager.show;


public class Home extends AppCompatActivity {
    public static ArrayList<Objects> objectsArrayList;
    public static ArrayList<Objects> arrayList;
    ArrayList<Queries> goals;
    RecyclerView rvNewgoals, rvMygoals;
    GoalsAdapter goalsAdapter;
    SavedAdapter savedAdapter;
    SharedPreferences sharedPreferences;
    LinearSnapHelper linearSnapHelper;
    AppCompatTextView cur_goals;
    MonthAdapter adapter;
    MaterialDialog dialog;
    MaterialDialog.Builder builder;
    Intent intent;

    public static ArrayList<Objects> getItems(Context context) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences("Items", MODE_PRIVATE);
        Gson gson = new Gson();
        String list = sharedPreferences.getString("Items", "");
        Type type = new TypeToken<ArrayList<Objects>>() {
        }.getType();
        arrayList = gson.fromJson(list, type);
        if (arrayList == null) {
            arrayList = new ArrayList<>();
        }
        return arrayList;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_home);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        rvNewgoals = findViewById(R.id.rvNewgoals);
        rvMygoals = findViewById(R.id.rvMygoals);
        linearSnapHelper = new LinearSnapHelper();
        cur_goals = findViewById(R.id.current_goals);

        //set up goals and their recyclerView
        goals = new ArrayList<>();
        goals.add(new Queries(getString(R.string.enterpreneurship), getString(R.string.ent_value), R.drawable.image_bulb, R.drawable.image_next, 0));
        goals.add(new Queries(getString(R.string.employability), getString(R.string.emp_value), R.drawable.image_brief, R.drawable.image_next, 0));
        goals.add(new Queries(getString(R.string.personal), getString(R.string.pes_value), R.drawable.image_heart, R.drawable.image_next, 0));
        goalsAdapter = new GoalsAdapter(this, goals);
        rvNewgoals.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        rvNewgoals.setAdapter(goalsAdapter);
        rvNewgoals.setNestedScrollingEnabled(false);
        rvNewgoals.addItemDecoration(new Divider(getResources().getDrawable(R.drawable.vector_recy_dv)));

        getMySavings(sharedPreferences.getString("Phone", ""));

        objectsArrayList.add(new Objects(16, "071923354800", "Entreprenuership", "Savvy Saver", "To buy cake", 2000, 200));
        objectsArrayList.add(new Objects(21, "071923354800", "Entreprenuership", "Savvy Saver", "To buy cake", 2500, 2000));
        objectsArrayList.add(new Objects(420, "071923354800", "Entreprenuership", "Savvy Saver", "To buy cake", 4000, 600));

        if (objectsArrayList != null && objectsArrayList.size() > 0) {
            savedAdapter = new SavedAdapter(objectsArrayList);
            rvMygoals.setVisibility(View.VISIBLE);
            rvMygoals.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
            rvMygoals.setAdapter(savedAdapter);
            rvMygoals.setNestedScrollingEnabled(false);
            cur_goals.setVisibility(View.VISIBLE);
            saveItems();
        }

        //add months into the months array list
        String[] months = new String[]{
                getString(R.string.january), getString(R.string.february), getString(R.string.march),
                getString(R.string.april), getString(R.string.may), getString(R.string.june),
                getString(R.string.july), getString(R.string.august), getString(R.string.september),
                getString(R.string.october), getString(R.string.november), getString(R.string.december)};

        // set up the months RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rvMonths);
        int numberOfColumns = 3;
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        adapter = new MonthAdapter(this, months);
        recyclerView.setAdapter(adapter);

        //check for incoming congratulatory message if present
        intent = getIntent();
        String goal = intent.getStringExtra("Goal");
        int saved = intent.getIntExtra("Saved", 0);
        String happyEmoji = ("\ud83d\ude03");
        if (intent.getExtras() != null && !goal.equals("")) {
            show(myToast("Congratulations, " + happyEmoji + " you have saved Naira " + saved + " towards your " + goal + " goal.", Gravity.BOTTOM, this).getView(), this);
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void getMySavings(String phone) {
        //This method fetches savings for the current user from the api endpoint
        objectsArrayList = new ArrayList<>();
        builder = new MaterialDialog.Builder(Home.this)
                .progress(true, 100)
                .content("Loading...")
                .cancelable(false);
        dialog = builder.build();
        dialog.show();

        AndroidNetworking.post(Helper.Header + "apicall=getmysavings")
                .addBodyParameter("phone", phone)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.cancel();
                        try {
                            JSONArray jsonArray = response.getJSONArray("savings");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Objects objects = new Objects(
                                        jsonObject.getInt("id"),
                                        jsonObject.getString("phone"),
                                        jsonObject.getString("goal"),
                                        jsonObject.getString("type"),
                                        jsonObject.getString("purpose"),
                                        jsonObject.getInt("saving"),
                                        jsonObject.getInt("saved")
                                );
                                objectsArrayList.add(objects);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (objectsArrayList != null && objectsArrayList.size() > 0) {
                            savedAdapter = new SavedAdapter(objectsArrayList);
                            rvMygoals.setVisibility(View.VISIBLE);
                            rvMygoals.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                            rvMygoals.setAdapter(savedAdapter);
                            rvMygoals.setNestedScrollingEnabled(false);
                            cur_goals.setVisibility(View.VISIBLE);
                            saveItems();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(getApplicationContext(), R.string.check_connection, Toast.LENGTH_LONG).show();
                        dialog.cancel();
                    }
                });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void saveItems() {
        //saves the items in the users table to sharedPreferences to enable offline notifications
        SharedPreferences mPrefs = getSharedPreferences("Items", MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = mPrefs.edit();

        String list = mPrefs.getString("Items", "");
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Objects>>() {
        }.getType();
        arrayList = gson.fromJson(list, type);
        if (arrayList == null) {
            arrayList = new ArrayList<>();
        }
        arrayList.clear();
        arrayList.addAll(objectsArrayList);

        list = gson.toJson(arrayList, type);
        prefsEditor.putString("Items", list);
        prefsEditor.putBoolean("Shown", true);
        prefsEditor.apply();

        //calls the alarm
        setRepeatingAlarm(getApplicationContext());
    }
}

package org.busaracenter.empowersavings.activities;

public class Helper {
    //static values to be used in different methods as parameters for the period the application is in
    //the foreground and not cleared from current tasks
    public static String Purpose = "";
    public static String Image = "";
    public static String Header = "http://34.218.243.82/savings-api/api/Api.php?";
    public static int Members = 0;
    public static int GroupSaving = 0;
    public static int GroupSaved = 0;
    public static int MySaved = 0;
    public static int MySaving = 0;
    public static int Position = 0;
    public static String Id = "";
    public static String Goal = "";
    public static String Type = "";
    public static String Phone = "";
    public static int Balance = 0;
}
